"""
Wire encoding exceptions
"""


class InvalidData(Exception):
    """
    A class which expresses invalid data was handled by the wire encoder
    """
    pass
