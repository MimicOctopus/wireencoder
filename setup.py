"""
Setup wireencoder
"""

import os

from setuptools import setup, find_packages
try:
    from pip._internal.req import parse_requirements
except ImportError:
    from pip.req import parse_requirements

def get_requirements(filename='requirements.txt'):
    requirements = os.path.join(os.path.dirname(__file__), filename)
    with open(requirements) as rf:
        data = rf.read()
    lines = map(lambda s: s.strip(), data.splitlines())
    return [l for l in lines if l and not l.startswith('#')]

setup(
    name='wire-encoder',
    version="1.0.4",
    author='Max Paradis',
    author_email='max@nullptrsolutions.ca',
    packages=find_packages(exclude=["tests"]),
    url='http://gitlab.com/MimicOctopus/wireencoder',
    license='MIT (see LICENSE)',
    description='A set of wire safe encoders',
    long_description=open('README.rst').read(),
    install_requires=["pybase64==0.2.1",
                      "zstandard>=0.8.1"],
    test_suite="tests",
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ]
)
