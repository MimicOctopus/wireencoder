"""
Just some sanity checks for the wireencode library
"""

import unittest
from wireencoder import ZstdBase64Encoder, ZstdBase64Decoder, \
                        ZlibBase64Encoder, ZlibBase64Decoder, \
                        ZlibBase85Encoder, ZlibBase85Decoder
from wireencoder.exceptions import InvalidData

TESTINPUT = "tests/input"

TESTZSTDB64 = "tests/zstdb64"

TESTZLIBB64 = "tests/zlibb64"

TESTZLIBB85 = "tests/zlibb85"

TESTB85 = "tests/b85"

def getFileContent(f: str) -> str:
    with open(f) as infile:
        return infile.read()

class TestZstdBase64(unittest.TestCase):

    def test_encode(self):
        z = ZstdBase64Encoder()
        i = getFileContent(TESTINPUT)
        expected = getFileContent(TESTZSTDB64)
        self.assertEqual(z.encode(i.encode('utf8')), expected)

    def test_encode_wrong(self):
        z = ZstdBase64Encoder()
        i = getFileContent(TESTINPUT)
        with self.assertRaises(TypeError):
            z.encode(i)

    def test_decode(self):
        z = ZstdBase64Decoder()
        i = getFileContent(TESTZSTDB64)
        expected = getFileContent(TESTINPUT)
        self.assertEqual(z.decode(i), expected.encode('utf8'))

    def test_decode_wrong_compress(self):
        z = ZstdBase64Decoder()
        i = getFileContent(TESTZLIBB64)
        with self.assertRaises(InvalidData):
            z.decode(i)

    def test_decode_wrong_encoding(self):
        z = ZstdBase64Decoder()
        i = getFileContent(TESTINPUT)
        with self.assertRaises(InvalidData):
            z.decode(i)

class TestZlibBase64(unittest.TestCase):

    def test_encode(self):
        z = ZlibBase64Encoder()
        i = getFileContent(TESTINPUT)
        expected = getFileContent(TESTZLIBB64)
        self.assertEqual(z.encode(i.encode('utf8')), expected)

    def test_encode_wrong(self):
        z = ZlibBase64Encoder()
        i = getFileContent(TESTINPUT)
        with self.assertRaises(TypeError):
            z.encode(i)

    def test_decode(self):
        z = ZlibBase64Decoder()
        i = getFileContent(TESTZLIBB64)
        expected = getFileContent(TESTINPUT)
        self.assertEqual(z.decode(i), expected.encode('utf8'))

    def test_decode_wrong_compress(self):
        z = ZlibBase64Decoder()
        i = getFileContent(TESTZSTDB64)
        with self.assertRaises(InvalidData):
            z.decode(i)

    def test_decode_wrong_encoding(self):
        z = ZlibBase64Decoder()
        i = getFileContent(TESTINPUT)
        with self.assertRaises(InvalidData):
            z.decode(i)

class TestZlibBase85(unittest.TestCase):

    def test_encode(self):
        z = ZlibBase85Encoder()
        i = getFileContent(TESTINPUT)
        expected = getFileContent(TESTZLIBB85)
        self.assertEqual(z.encode(i.encode('utf8')), expected)
    
    def test_encode_wrong(self):
        z = ZlibBase85Encoder()
        i = getFileContent(TESTINPUT)
        with self.assertRaises(TypeError):
            z.encode(i)

    def test_decode(self):
        z = ZlibBase85Decoder()
        i = getFileContent(TESTZLIBB85)
        expected = getFileContent(TESTINPUT)
        self.assertEqual(z.decode(i), expected.encode('utf8'))

    def test_decode_wrong_compress(self):
        z = ZlibBase85Decoder()
        i = getFileContent(TESTB85)
        with self.assertRaises(InvalidData):
            z.decode(i)

    def test_decode_wrong_encoding(self):
        z = ZlibBase85Decoder()
        i = getFileContent(TESTZSTDB64)
        with self.assertRaises(InvalidData):
            z.decode(i)

if __name__ == '__main__':
    unittest.main()
